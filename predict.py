from model import *
from data import *
testGene = testGenerator("data/membrane/test")
model = unet()
model.load_weights("unet_membrane.hdf5")
results = model.predict_generator(testGene,30,verbose=1) # 30 denotes the number of images in the test folder(for prediction)
saveResult("data/membrane/test",results)
